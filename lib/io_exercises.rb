# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  @cump_number = 1 + rand(100)
  guessed = false
  counter = 0
  until guessed
    guessed = process_guess(prompt)
    counter +=1
  end
  congrats(counter)
end


def prompt
  puts "guess a number"
  players_guess = gets.chomp.to_i
end

def process_guess(guess)
  if guess > @cump_number
    puts "#{guess} is too high"
    return false
  elsif guess < @cump_number
    puts "#{guess} is too low"
    return false
  end
  true
end

def congrats(counter)
   puts "you got it in #{counter} times. it was #{@cump_number}. good job"
end

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def shuffle_file
  file_name = prompt_for_file
  line_array = File.readlines(file_name).map(&:chomp)
  shuffle_lines = line_array.shuffle
  shuffled_file_name =  "#{file_name}-shuffled.txt"
  File.open(shuffled_file_name, 'w') do |f|
    f.write shuffle_lines.join("\n")
  end
end

def prompt_for_file
  puts "Please enter a file name"
  gets.chomp
end
